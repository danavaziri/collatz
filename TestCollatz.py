#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, calculate_cycle

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "5 29\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5)
        self.assertEqual(j, 29)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(106, 678)
        self.assertEqual(v, 145)

    def test_eval_2(self):
        v = collatz_eval(316, 911)
        self.assertEqual(v, 179)

    def test_eval_3(self):
        v = collatz_eval(256, 184)
        self.assertEqual(v, 128)

    def test_eval_4(self):
        v = collatz_eval(884, 995)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(760, 144)
        self.assertEqual(v, 171)

    def test_eval_6(self):
        v = collatz_eval(832, 184)
        self.assertEqual(v, 171)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 832, 184, 171)
        self.assertEqual(w.getvalue(), "832 184 171\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("832 184\n760 144\n256 184\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "832 184 171\n760 144 171\n256 184 128\n")

    # -----
    # calculateCycle
    # -----
    def test_calculate_cycle_1(self):
        cache = {}
        self.assertEqual(calculate_cycle(1, cache), 1)

    def test_calculate_cycle_2(self):
        cache = {}
        self.assertEqual(calculate_cycle(2, cache), 2)

    def test_calculate_cycle_3(self):
        cache = {}
        self.assertEqual(calculate_cycle(3, cache), 8)

    def test_calculate_cycle_4(self):
        cache = {}
        self.assertEqual(calculate_cycle(4527, cache), 39)

    def test_calculate_cycle_5(self):
        cache = {}
        self.assertEqual(calculate_cycle(300, cache), 17)

    def test_calculate_cycle_6(self):
        cache = {}
        self.assertEqual(calculate_cycle(452625, cache), 157)

# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()
