#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

from typing import IO, List

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    cache will hold on to the cycle lengths of
    diffrent numbers as they get read in and
    computed
    """

    cache = {}
    cache[1] = 1
    max_cycle = 0
    if i > j:
        i, j = j, i
    if i < (j // 2):
        i = j // 2
    for k in range(i, j + 1):
        if k not in cache.keys():
            this_cycle = calculate_cycle(k, cache)
        else:
            this_cycle = cache[i]
        if this_cycle > max_cycle:
            max_cycle = this_cycle
    assert max_cycle > 0
    return max_cycle


# ------------
# calculateCycle
# ------------

def calculate_cycle(i, cache):
    """
    helper function to help calculate
    cycle length. Got help from in
    class code. It wil also update
    cache as it calculates cycle
    length values.
    """
    assert i > 0
    if i in cache.keys():
        return cache[i]
    count = 1
    savedi = i
    while i > 1:
        if i in cache.keys():
            cache[savedi] = cache[i] + count - 1
            return cache[savedi]
        if i % 2 == 0:
            i = i // 2
            count += 1
        else:
            i = ((i * 3) + 1) // 2
            count += 2
    cache[savedi] = count
    assert count > 0
    return count


# -------------
# collatz_print
# -------------

def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
